FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*with-dependencies.jar
COPY ${JAR_FILE} app.jar
COPY src/main/resources src/main/resources
EXPOSE 8084
ENTRYPOINT ["java","-jar","/app.jar"]